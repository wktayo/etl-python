import sys
import pandas
import requests
import csv
from pathlib import Path
import logging


import logging.config

logging.config.fileConfig(fname='../util/logging_to_file.conf')
logger = logging.getLogger(__name__)

def Top20(inputdata):
    logger.info("load top 20 begin ..")
    try:
        top20 = inputdata.sort_values("priceUsd", ascending=False).head(20)
        filepath_top20 = Path("../../../data/df_top20.csv")
        filepath_top20.parent.mkdir(parents=True, exist_ok=True)
        df_top20 = top20.to_csv(filepath_top20)

    except Exception as e:
        logger.info("loading top 20 failled Please check " + str(e), exc_info=True)
        sys.exit(1)
    else:
        logger.info("Everything is good with loadind Top 20")

        return df_top20


def worst20(inputdata):
    logger.info("loadind worst 20 begin ...")
    try:
        worst20 = inputdata.sort_values("priceUsd", ascending=True).head(20)
        filepath_worst20 = Path("../../../data/df_worst20.csv")
        filepath_worst20.parent.mkdir(parents=True, exist_ok=True)
        df_worst20 = worst20.to_csv(filepath_worst20)

    except Exception as e:
        logger.error("loading worst 20 failled Please check " + str(e), exc_info=True)
    else:
        logger.info("Everything is good with loading worst 20")

    return df_worst20

def crypto_with_a(input):
    try:
        df_a = input[input["name"].str.contains('a')]
        filepath_df_a = Path("../../../data/df_a.csv")
        filepath_df_a.parent.mkdir(parents=True, exist_ok=True)
        df_out = df_a.to_csv(filepath_df_a)

    except Exception as e:
        logger.error("loading crypto with a failled Please check " + str(e), exc_info=True)
    else:
        logger.info("Everything is good with loading df with a")

    return df_out