import unittest
from ingestion import ingestion

class TestIngestionFunction(unittest.TestCase):

    def test_valid_url(self):
        # Test avec une URL valide (simulée)
        fake_url = "http://api.coincap.io/v2/assets"
        result = ingestion(fake_url)
        self.assertTrue(result)  # Vérifie que la fonction renvoie des données (not False)

    def test_invalid_url(self):
        # Test avec une URL invalide (simulée)
        invalid_url = "google.com"
        result = ingestion(invalid_url)
        self.assertEqual(result, [])  # Vérifie que la fonction renvoie une liste vide

    # Vous pouvez ajouter d'autres méthodes de test pour tester d'autres scénarios

if __name__ == '__main__':
    unittest.main()