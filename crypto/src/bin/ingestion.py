import sys
import pandas
import requests
import csv
import logging
import logging.config
import validators

logging.config.fileConfig(fname='../util/logging_to_file.conf')
logger = logging.getLogger(__name__)

url_crypto = 'http://api.coincap.io/v2/assets'

headers = {
    "Accept": "application/json",
    "Content-Type": "application/json"
}
ourdata = []
def ingestion(x):
    try:
        url = validators.url(x)
        if url:
            logger.info("url valid")
            logger.info("start ingestion .....")
            response = requests.request("GET", url=x, headers=headers, data={})
            myjson = response.json()


            for i in myjson['data']:
                listing = [i['symbol'], i['name'], i['priceUsd'], i['rank'], i['supply']]
                ourdata.append(listing)
        else:
            logger.info("url invalid")
    except Exception as e:
        logger.error("Error in the ingestion - Please check trace ." + str(e), exc_info=True)
        sys.exit(1)

    else:
        logger.info("complete ingestion ....")

    return ourdata

def bonj (x):
    print("hi", x)