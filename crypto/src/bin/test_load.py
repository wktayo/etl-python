import unittest
import pandas as pd
from pathlib import Path
from load import Top20, worst20, crypto_with_a  # Remplacez 'your_module' par le nom de votre module

class TestCryptoFunctions(unittest.TestCase):

    def setUp(self) -> None:
        self.input_data = pd.DataFrame({
            'symbol': ['BTC', 'ETH', 'XRP', 'LTC', 'BCH'],
            'name': ['Bitcoin', 'Ethereum', 'XRP', 'Litecoin', 'Bitcoin Cash'],
            'priceUsd': [35000, 2000, 1.5, 100, 450],
            'rank': [1, 2, 3, 4, 5]
        })

    def test_top20(self):
        # Test avec des données d'entrée simulées
        result = Top20(self.input_data)
        self.assertTrue(Path("../../../data/df_top20.csv").is_file())  # Vérifie que le fichier a été créé

    def test_worst20(self):
        # Test avec des données d'entrée simulées
        result = worst20(self.input_data)
        self.assertTrue(Path("../../../data/df_worst20.csv").is_file())  # Vérifie que le fichier a été créé

    def test_crypto_with_a(self):
        # Test avec des données d'entrée simulées
        result = crypto_with_a(self.input_data)
        self.assertTrue(Path("../../../data/df_a.csv").is_file())  # Vérifie que le fichier a été créé

if __name__ == '__main__':
    unittest.main()