import pandas as pd
from pathlib import Path
import validators
import csv

df = pd.DataFrame([['tayo' ,6,7,8],
                   ['pores' ,9,12,14],
                   ['papa' ,8,10,6]], columns = ['x','b','c','d'])

df_a = df[df["x"].str.contains("a")]

filepath = Path("../../../data/df_a.csv")

filepath.parent.mkdir(parents=True, exist_ok=True)

df_a.to_csv(filepath)

def val (url):
    url_out = validators.url(url)
    if url_out:
        print("URL is valid")
    else:
        print("URL is invalid")


if __name__=="__main__":
    val("http://api.coincap.io/v2/assets")
