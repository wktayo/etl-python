import sys
import pandas
import requests
import csv
import logging

import logging.config

logging.config.fileConfig(fname='../util/logging_to_file.conf')
logger = logging.getLogger(__name__)

def transfomation(inputdata):
    logger.info("start transformation.....")
    try:
        outputdata1 = pandas.DataFrame(inputdata, columns=['symbol','name','priceUsd','rank','supply'])
        outputdata1["supply"] = outputdata1["supply"].astype(float, errors="raise")
        outputdata1["supply"] = outputdata1["supply"].round(decimals=2)
        outputdata1["priceUsd"] = outputdata1["priceUsd"].astype(float, errors="raise")
        outputdata1["priceUsd"] = outputdata1["priceUsd"].round(decimals=2)

    except Exception as e:

        logger.error("we have a probleme with the transformation function please check trace" + str(e), exc_info=True)
        sys.exit(1)

    else:
        logger.info("Everything is good with the transformation")

    return outputdata1