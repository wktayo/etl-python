import sys

import requests
import csv
import logging
import logging.config
from ingestion import ingestion
from transformation import transfomation
from load import worst20, Top20, crypto_with_a

logging.config.fileConfig(fname='../util/logging_to_file.conf')

url = 'http://api.coincap.io/v2/assets'


def main():
    try:
        logging.info("main() is started ...")
        outdata = ingestion(url)
        outP = transfomation(outdata)
        T = Top20(outP)
        W = worst20(outP)
        df_a = crypto_with_a(outP)
        print(T)
    except Exception as e:
        logging.error("Error with the main() + Please tcheck " +str(e), exc_infro=True)
        sys.exit(1)


if __name__=="__main__":
    logging.info("Pipeline start ....")
    main()
