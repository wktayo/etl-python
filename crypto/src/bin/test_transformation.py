import unittest
import pandas as pd
from transformation import transfomation  # Remplacez 'your_module' par le nom de votre module

class TestTransformationFunction(unittest.TestCase):

    def test_transformation_rounding(self):
        # Test avec des données d'entrée où les colonnes 'supply' et 'priceUsd' ont plusieurs décimales
        input_data = [
            ['BTC', 'Bitcoin', 35000.12345, 1, 18798765.4321],
            ['ETH', 'Ethereum', 2000.98765, 2, 12345678.9876]
        ]
        result = transfomation(input_data)
        expected_output = pd.DataFrame(input_data, columns=['symbol', 'name', 'priceUsd', 'rank', 'supply'])
        expected_output['supply'] = expected_output['supply'].round(decimals=2)
        expected_output['priceUsd'] = expected_output['priceUsd'].round(decimals=2)
        self.assertTrue(result.equals(expected_output))  # Vérifie que les DataFrames sont égaux

    def test_transformation_type_conversion(self):
        # Test avec des données d'entrée où les colonnes 'supply' et 'priceUsd' sont des chaînes de caractères
        input_data = [
            ['BTC', 'Bitcoin', '35000.12345', 1, '18798765.4321'],
            ['ETH', 'Ethereum', '2000.98765', 2, '12345678.9876']
        ]
        result = transfomation(input_data)
        expected_output = pd.DataFrame(input_data, columns=['symbol', 'name', 'priceUsd', 'rank', 'supply'])
        expected_output['supply'] = expected_output['supply'].astype(float)
        expected_output['supply'] = expected_output['supply'].round(decimals=2)
        expected_output['priceUsd'] = expected_output['priceUsd'].astype(float)
        expected_output['priceUsd'] = expected_output['priceUsd'].round(decimals=2)
        self.assertTrue(result.equals(expected_output))  # Vérifie que les DataFrames sont égaux

    # Vous pouvez ajouter d'autres méthodes de test pour tester d'autres scénarios

if __name__ == '__main__':
    unittest.main()