import sys
import unittest

from ..bin.ingestion import ingestion

class TestFonctionIngestion(unittest.TestCase):

    def test_url_valide(self):
        url_valide = "http://api.coincap.io/v2/assets"
        self.assertTrue(ingestion(url_valide))

    def test_url_invalide(self):
        url_invalide = "http://api.coincap.io/v2/asset"
        self.assertFalse(ingestion(url_invalide))

    def test_url_avec_parametres(self):
        url_avec_parametres = "http://api.coincap.io/v2/assets"
        self.assertTrue(ingestion(url_avec_parametres))

if __name__ == '__main__':
    unittest.main()