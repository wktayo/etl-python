import sys
import unittest
import pandas as pd
from ..bin import transformation as transfomation

class TestTransformationFonction(unittest.TestCase):
    def test_somme_liste_de_listes(self):
        # Test avec une liste vide
        liste_vide = [0, 2]
        self.assertEqual(transfomation(liste_vide), 0)

        # Test avec une liste de listes contenant des nombres entiers
        liste_int = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        self.assertEqual(transfomation(liste_int), 45)

        # Test avec une liste de listes contenant des nombres décimaux
        liste_float = [[1.1, 2.2], [3.3, 4.4, 5.5]]
        self.assertAlmostEqual(transfomation(liste_float), 16.5, places=2)


if __name__ == '__main__':
    unittest.main()